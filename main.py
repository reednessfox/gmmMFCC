import warnings
warnings.filterwarnings('ignore')

# ====================================================================================
# READ FROM HDF5
# ====================================================================================

import h5py
import numpy as np

emo = []

def read_from_hdf5(filename):
    with h5py.File(filename, 'r') as hdf:
        bs_items = list(hdf.items())
        print('items:\n', bs_items, '\n')

        grp1 = hdf.get('features_emo')
        grp1_items = list(grp1.items())

        rab = np.array(grp1.get('anger'))
        dis = np.array(grp1.get('disgust'))
        pau = np.array(grp1.get('fear'))
        gio = np.array(grp1.get('joy'))
        neu = np.array(grp1.get('neural'))
        tri = np.array(grp1.get('sadness'))
        sor = np.array(grp1.get('surprise'))
    emo.append(rab)
    emo.append(dis)
    emo.append(pau)
    emo.append(gio)
    emo.append(neu)
    emo.append(tri)
    emo.append(sor)

    for i in range(len(emo)):
        print(emo[i].shape)

# ====================================================================================
# DIVISION ON TEST AND TRAIN
# ====================================================================================
#  test (0.3) and train (0.7)

from sklearn.model_selection import train_test_split

X_test = []
y_test = []
X_train = []
y_train = []

def division_test_and_train(emo_matrix):
    size_test = int(emo_matrix.shape[0]*0.3)
    size_train = int(emo_matrix.shape[0]*0.7)

    test_indxs = np.random.randint(emo_matrix.shape[0], size=size_test)
    train_indxs = np.random.randint(emo_matrix.shape[0], size=size_train)
    _test = []
    _train = []

    for i in range(test_indxs.shape[0]):
        indx = test_indxs[i]
        _test.append(emo_matrix[indx])

    for i in range(train_indxs.shape[0]):
        indx = train_indxs[i]
        _train.append(emo_matrix[indx])

    _test = np.array(_test)
    _train = np.array(_train)

    X_train = _train[:, 0:39]
    y_train = _train[:, 39].reshape(-1,1)

    X_test = _test[:, 0:39]
    y_test = _test[:, 39].reshape(-1,1)
      
    return X_train, y_train, X_test, y_test

def stack_test(X_test, y_test):
    tmp1 = X_test[0]
    tmp2 = X_test[1]
    X_stck = np.vstack((tmp1, tmp2))

    for i in range(2, len(X_test)):
        tmp1 = X_test[i]
        X_stck = np.vstack((X_stck, tmp1))
    tmp1 = y_test[0]
    tmp2 = y_test[1]
    y_stck = np.vstack((tmp1, tmp2))

    for i in range(2, len(X_test)):
        tmp1 = y_test[i]
        y_stck = np.vstack((y_stck, tmp1))

    return X_stck, y_stck

read_from_hdf5('results/mfcc/emovodb.h5')
print()

for i in range(len(emo)):
    _X_train, _y_train, _X_test, _y_test = division_test_and_train(emo[i])

    X_test.append(_X_test)
    y_test.append(_y_test)
    X_train.append(_X_train)
    y_train.append(_y_train)

X_test, y_test = stack_test(X_test, y_test)

for i in range(len(X_train)):
    print('X_train shape:', X_train[i].shape, 'y_train shape:', y_train[i].shape)
print('X test:', X_test.shape, 'y test:', y_test.shape)

import warnings
warnings.filterwarnings('ignore')

# ====================================================================================
# TRAIN GAUSSIAN MUXTURE MODEL
# ====================================================================================
#   components: 100/500/1000
#   covariance_type: diagonal
#   models: 7
#   max_iter: 1/5/10/20/50/100 (default: 100)
# ====================================================================================
#  confusion matrix

from sklearn import metrics

itr = [ 1, 5, 10, 20, 50, 100 ]
comp = [ 100, 500, 1000 ]

title_list = []
filenames = []
for i in range(len(itr)):
    for j in range(len(comp)):
        title = u'GMM: iter ' + str(itr[i]) + ', components ' + str(comp[j])
        title_list.append(title)

for i in range(len(title_list)):
    filename = 'results/mfcc/confusionMatrix/' + title_list[i] + '.png'
    filenames.append(filename)
def calculate_results(scrs, filenames, ttl):
    frst = scrs[0].reshape(-1,1)
    scnd = scrs[1].reshape(-1,1)
    a = np.hstack((frst, scnd))

    for i in range(2, len(scrs)):
        frst = scrs[i].reshape(-1,1)
        a = np.hstack((a, frst))
    
    Pred_cnfmtrx = []

    for i in range(a.shape[0]):
        b = np.argmax(a[i])
        Pred_cnfmtrx.append(b)

    Pred_cnfmtrx = np.array(Pred_cnfmtrx)
    print(Pred_cnfmtrx, Pred_cnfmtrx.shape)

    True_cnfmtrx = []

    for i in range(y_test.shape[0]):
        tmp = y_test[i].astype(np.int64)
        True_cnfmtrx.append(tmp)
    True_cnfmtrx = np.array(True_cnfmtrx).reshape(-1)
    print(True_cnfmtrx, True_cnfmtrx.shape)
    print()
    print('accuracy score:', metrics.accuracy_score(y_true=True_cnfmtrx, y_pred=Pred_cnfmtrx))
    print('f1 score <macro>:', metrics.f1_score(y_true=True_cnfmtrx, y_pred=Pred_cnfmtrx, average='macro'))
    print('f1 score <micro>:', metrics.f1_score(y_true=True_cnfmtrx, y_pred=Pred_cnfmtrx, average='micro'))

    cm = metrics.confusion_matrix(True_cnfmtrx, Pred_cnfmtrx)

    plt.figure(figsize=(12,8))
    plt.title(ttl)
    sns.heatmap(cm, square=True, annot=True, cbar=False)
    plt.xlabel('predicted value')
    plt.ylabel('true value')

    plt.show()

def calculate_results_percent(scrs, filenames, ttl):
    frst = scrs[0].reshape(-1,1)
    scnd = scrs[1].reshape(-1,1)
    a = np.hstack((frst, scnd))

    for i in range(2, len(scrs)):
        frst = scrs[i].reshape(-1,1)
        a = np.hstack((a, frst))

    Pred_cnfmtrx = []
    for i in range(a.shape[0]):
        b = np.argmax(a[i])
        Pred_cnfmtrx.append(b)
    Pred_cnfmtrx = np.array(Pred_cnfmtrx)
    print(Pred_cnfmtrx, Pred_cnfmtrx.shape)

    True_cnfmtrx = []

    for i in range(y_test.shape[0]):
        tmp = y_test[i].astype(np.int64)
        True_cnfmtrx.append(tmp)

    True_cnfmtrx = np.array(True_cnfmtrx).reshape(-1)
    print(True_cnfmtrx, True_cnfmtrx.shape)
    
    print()
    
    print('accuracy score:', metrics.accuracy_score(y_true=True_cnfmtrx, y_pred=Pred_cnfmtrx))
    print('f1 score <macro>:', metrics.f1_score(y_true=True_cnfmtrx, y_pred=Pred_cnfmtrx, average='macro'))
    print('f1 score <micro>:', metrics.f1_score(y_true=True_cnfmtrx, y_pred=Pred_cnfmtrx, average='micro'))

    cm = metrics.confusion_matrix(True_cnfmtrx, Pred_cnfmtrx)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    plt.figure(figsize=(12,8))
    plt.title(ttl)
    sns.heatmap(cm, square=True, annot=True, cbar=False)
    plt.xlabel('predicted value')
    plt.ylabel('true value')
    plt.savefig(filename)
    plt.show()

import warnings
warnings.filterwarnings('ignore')

# ====================================================================================
#  train gaussian mixture models
# ====================================================================================

from sklearn import mixture

models = []
scores = []

count = 0

for i in range(len(itr)):
    for j in range(len(comp)):
        print('num iteration:', itr[i], '| num componets:', comp[j])
        mdls = []
        scrs = []

        for k in range(len(X_train)):
            gmm = mixture.GaussianMixture(n_components=comp[j], covariance_type='diag', max_iter=itr[i])
            mod = gmm.fit(X_train[k])
            scor = gmm.score_samples(X_test).reshape(-1,1)
            mdls.append(mod)
            scrs.append(scor)

            print(' train progress:', k+1, 'in', len(X_train))
        print()
        print('ACCURACY:')   

        calculate_results(scrs, filenames[count], title_list[count])
        calculate_results_percent(scrs, filenames[count], title_list[count])

        print()
        print()

        models.append(mdls)
        scores.append(scrs)

        count += 1

XgmmScores = scores[0][0]

for i in range(len(scores[0])-1):
    XgmmScores = np.hstack((XgmmScores, scores[0][i+1]))

print(XgmmScores.shape, y_test.shape)

np.savetxt('results/mfcc/XgmmScores.csv', XgmmScores, delimiter=',')
np.savetxt('results/mfcc/ygmmScores.csv', y_test, delimiter=',')
